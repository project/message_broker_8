<?php

namespace Drupal\message_broker_8;

/**
 * General exception which is thrown by the module in case something went wrong.
 */
class MessageBrokerException extends \Exception {
}
