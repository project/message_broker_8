<?php

namespace Drupal\message_broker_8;

/**
 * An exception a consumer should throw to indicate a critical error.
 *
 * This means that application is not able
 * to proceed at this state.
 * The message broker infrastructure catches these exceptions,
 * terminates the process, allowing you to restart the process.
 */
class CriticalErrorException extends \Exception {
}
