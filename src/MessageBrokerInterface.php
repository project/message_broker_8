<?php

namespace Drupal\message_broker_8;

/**
 * Represents the message broker.
 *
 * May be implemented via AQMP or any other protocol.
 */
interface MessageBrokerInterface {

  /**
   * Sends a message to the message broker.
   *
   * @param string $body
   *   Message body.
   * @param string $destination
   *   Destination of message.
   * @param array $options
   *   Further message options.
   */
  public function sendMessage($body, $destination, array $options = []);

}
