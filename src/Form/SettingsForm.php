<?php

namespace Drupal\message_broker_8\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure message broker.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'message_broker_8_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'message_broker_8.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::config('message_broker_8.settings');
    $implementations = _message_broker_8_get_implementations();
    $options = [];

    foreach ($implementations as $key => $settings) {
      $options[$key] = $settings['title'];

      if (!empty($settings['description'])) {
        $options[$key] .= ' <i> - ' . $settings['description'] . '</i>';
      }
    }

    $form['intro'] = [
      '#markup' => '<p>' . t('Choose the implementation you
      want to use for sending and receiving messages.<br />Visit the configuration
      page for the chosen implementation to see more options.') . '</p>',
    ];

    $form['message_broker_8_implementation'] = [
      '#type' => 'radios',
      '#options' => $options,
      '#title' => t('Message broker implementation'),
      '#default_value' => $config->get('message_broker_8_implementation'),
    ];

    $form['message_broker_self'] = [
      '#type' => 'textfield',
      '#title' => t('Instance name'),
      '#description' => t('This value gets passed to the consumers hook,
      enabling modules to consume queues that contain this name.'),
      '#default_value' => $config->get('message_broker_self'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('message_broker_8.settings')
      ->set('message_broker_8_implementation', $form_state->getValue('message_broker_8_implementation'))
      ->set('message_broker_self', $form_state->getValue('message_broker_self'))
      ->save();
  }

}
