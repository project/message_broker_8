<?php

namespace Drupal\message_broker_8;

/**
 * An exception a consumer should throw to indicate that a message is invalid.
 *
 * This message broker module will catch the exception, ack the
 * message and call a invalidMessageHandler.
 */
class InvalidMessageException extends \Exception {
}
