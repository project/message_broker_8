The module contains functions for creating and switching between different AMPQ message broker implementations.

- Consumer hook - used for receiving messages
- Implementation hook
- Configuration form

Requirements
===============
- PHP 5.6
- AMQP compliant message broker

Installation
===============
Install the module as usual, more info can be found on:
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
